class Dictionary
  def initialize
    @d = {}
  end

  def entries
    @d
  end

  def add(entry)
    if entry.class == Hash
      @d[entry.keys[0]] = entry.values[0]
    else
      @d[entry] = nil
    end
  end

  def keywords
    @d.keys.sort
  end

  def include?(key)
    @d.keys.include?(key)
  end

  def find(substr)
    output = {}
    @d.keys.each do |key|
      output[key] = @d[key] if key =~ /#{substr}/
    end
    output
  end

  def printable
    output = ''
    @d.sort.each do |k, v|
      output += "[#{k}] \"#{v}\""
      output += "\n" if k != @d.keys.sort.last
    end
    output
  end
end
