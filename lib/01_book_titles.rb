class Book
  attr_reader :title

  def title=(title)
    # Don't capitalize articles, conjuctions, prepositions
    dont_capitalize = %w[a an the for and nor but yet so in of]
    @title = title.split(' ').map.with_index do |word, idx|
      if dont_capitalize.include?(word) && idx != 0
        word
      else
        word.capitalize
      end
    end
                  .join(' ')
  end
end
