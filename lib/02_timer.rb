class Timer
  attr_accessor :seconds, :minutes, :hours
  def initialize
    @seconds = 0
    @minutes = 0
    @hours = 0
  end

  def time_string
    while @seconds >= 60
      @seconds -= 60
      @minutes += 1
    end
    while @minutes >= 60
      @minutes -= 60
      @hours += 1
    end
    h_padding = '0' if @hours < 10
    m_padding = '0' if @minutes < 10
    s_padding = '0' if @seconds < 10
    "#{h_padding}#{@hours}:#{m_padding}#{@minutes}:#{s_padding}#{@seconds}"
  end
end
